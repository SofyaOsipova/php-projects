<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $example = '22 * X = 220;';
        $nums = explode(' ', $example);
        if ($nums[0] == 'X') {
            if ($nums[1] == '+') {
                $answer = (float)$nums[4] - (float)$nums[2];
            } elseif ($nums[1] == '-') {
                $answer = (float)$nums[4] + (float)$nums[2];
            } elseif ($nums[1] == '*') {
                $answer = (float)$nums[4] / (float)$nums[2];
            } elseif ($nums[1] == '/') {
                $answer = (float)$nums[4] * (float)$nums[2];
            }
        } elseif ($nums[2] == 'X') {
            if ($nums[1] == '+') {
                $answer = (float)$nums[4] - (float)$nums[0];
            } elseif ($nums[1] == '-') {
                $answer = (float)$nums[4] + (float)$nums[0];
            } elseif ($nums[1] == '*') {
                $answer = (float)$nums[4] / (float)$nums[0];
            } elseif ($nums[1] == '/') {
                $answer = (float)$nums[4] * (float)$nums[0];
            }
        } elseif ($nums[4] == 'X') {
            if ($nums[1] == '+') {
                $answer = (float)$nums[0] - (float)$nums[2];
            } elseif ($nums[1] == '-') {
                $answer = (float)$nums[0] + (float)$nums[2];
            } elseif ($nums[1] == '*') {
                $answer = (float)$nums[0] * (float)$nums[2];
            } elseif ($nums[1] == '/') {
                $answer = (float)$nums[0] / (float)$nums[2];
            }
        }
        echo $answer;
    ?>
</body>
</html>