<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img src="img/logo.png" alt="logo" width="250px">
        <h1>Hello, world!</h1>
    </header>
    <main>
        <?php
            echo "<p>Hello, word!</p>";
        ?>
    </main>
    <footer>
        <p>Создать веб-страницу с динамическим контентом. Загрузить код в удаленный репозиторий. Залить на хостинг.</p>
    </footer>
</body>
</html>