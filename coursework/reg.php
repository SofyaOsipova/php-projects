<?php
include 'add.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Coursework</title>
</head>
<body>
    <section class="main-part">
        <header class="header">
            <ul class="header-list">
                <li class="header-item"><a href="reg.php" class="header-link">Создание Канала</a></li>
                <li class="header-item"><a href="list.php" class="header-link">Список Каналов</a></li>
                <li class="header-item"><a href="like.php" class="header-link">Доверенные Каналы</a></li>
            </ul>
        </header>
        <section class="reg-channel">
            <form action="" method="post" class="reg-form">
                <div class="reg-labels">
                    <label for="channel" class="reg-label">Название канала</label>
                    <input name="title" id="channel" type="text" class="reg-input" aria-label="Название Канала">
                </div>
                <div class="reg-labels">
                    <label for="editor" class="reg-label">Описание канала</label>
                    <textarea name="content" id="editor" class="reg-input" rows="6"></textarea>
                </div>
                <div class="reg-checkbox">
                    <input name="like" class="reg-check" type="checkbox" value="1" id="flexCheckChecked" checked>
                    <label class="form-check-label" for="flexCheckChecked">
                        Доверенный
                    </label>
                </div>
                <div class="">
                    <button name="create-channel" class="create-button" type="submit">Создать</button>
                </div>
            </form>
        </section>
    </section>
</body>
</html>