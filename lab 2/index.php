<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img class="logo" width="20%" height="20%" src="logo.png" alt="mospolytech">
        <h1>Feedback form</h1>
    </header>

    <main>
        <form action="https://httpbin.org/post" method="POST">
            <label for="name-id">Имя</label>
            <input type="text" name="name" id="name-id">
            <label for="email-id">E-mail пользователя</label>
            <input type="email" name="email" id="email-id">
            <label for="select-id">Тип обращения</label>
            <select name="select" id="select-id">
                <option value="Жалоба">Жалоба</option>
                <option value="Предложение">Предложение</option>
                <option value="Благодарность">Благодарность</option>
            </select> 
            <label for="appeal-id">Текст обращения</label>
            <input type="text" name="appeal" id="appeal-id">
            <p>Вариант ответа</p>
            <div class="variant-wrapper">
                <input type="checkbox" value="SMS" name="variant" id="SMS">
                <label for="SMS">SMS</label>
                <input type="checkbox" value="e-mail" name="variant" id="e-mail">
                <label for="e-mail">E-mail</label>
            </div>
            <input type="submit"></input>
            <a href="getheaders.php">2 страница</a>
        </form>
    </main>

    <footer>
        <p>Собрать сайт из двух страниц.</p>
    </footer>
</body>
</html>
